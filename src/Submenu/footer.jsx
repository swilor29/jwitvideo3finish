import check from "../assets/check.png";
import error from "../assets/error.png";
import refresh from "../assets/refresh.png";

const Item = ({ image, onClick }) => {
  return (
    <img className="img_circle " src={image} alt="check" onClick={onClick} />
  );
};

const Footer = ({ version, onReset, onCancel, onAcept }) => {
  const handleCancel = () => {
    if (typeof onCancel === "function") {
      console.log("...");
    }
  };
  const handleAcept = () => {
    if (typeof onAcept === "function") {
      console.log("...");
    }
  };
  const handleReset = () => {
    if (typeof onReset === "function") {
      onReset();
    }
  };
  if (version === 1) {
    let data = [
      { icon: error, onClick: handleCancel },
      { icon: check, onClick: handleAcept },
    ];
    return (
      <footer className="footer">
        {data.map((v, i) => (
          <Item image={v.icon} key={i} onClick={v.onClick} />
        ))}
      </footer>
    );
  }
  if (version === 2) {
    let data = [
      { icon: error, onClick: handleCancel },
      { icon: check, onClick: handleAcept },
      { icon: refresh, onClick: handleReset },
    ];
    return (
      <footer className="footer">
        {data.map((v, i) => (
          <Item image={v.icon} key={i} onClick={v.onClick} />
        ))}
      </footer>
    );
  }
};

export default Footer;
