import Options from "./options";
import Footer from "./footer";
const App = ({ onChange, color, id, footer, options, onReset }) => {
  const handleChange = (value) => {
    if (typeof onChange === "function") onChange(value, id);
  };
  const handleReset = () => {
    if (typeof onReset === "function") onReset(id);
  };

  return (
    <div className="hero" style={{ backgroundColor: color }}>
      {options?.map((option, index) => (
        <Options key={index} {...option} onChange={handleChange} />
      ))}

      <Footer version={footer} onReset={handleReset} />
    </div>
  );
};

export default App;
