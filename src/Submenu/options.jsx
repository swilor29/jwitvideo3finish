import check from "../assets/check.png";

const Options = ({ title, state, onChange }) => {
  const handleChange = () => {
    if (typeof onChange === "function") onChange(title);
  };

  return (
    <div className="hero_content">
      <p className="options_title">{title}</p>
      <div
        className={`hero_content_circle ${
          state ? "circle-white" : "circle-transparent"
        }`}
        onClick={handleChange}
      >
        <img
          className={`img_circle ${state ? "show" : "hidden"}`}
          src={check}
          alt="img"
        />
      </div>
    </div>
  );
};

export default Options;
