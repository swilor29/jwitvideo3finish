import { useState } from "react";
import "./App.css";
import Card from "./components/Card";
import Submenu from "./Submenu";
function App() {
  const [data, setData] = useState([
    {
      id: 1,
      color: "#21d0d0",
      options: [
        {
          title: "PRICE LOW TO HIGHT",
          state: false,
        },
        {
          title: "PRICE HIGHT TO LOW",
          state: false,
        },
        {
          title: "POPULARITY",
          state: true,
        },
      ],
      footer: 1,
    },
    {
      id: 2,
      color: "#ff7745",
      options: [
        {
          title: "Opcion 1",
          state: false,
        },
        {
          title: "Opcion 2",
          state: false,
        },
        {
          title: "Opscion 3",
          state: true,
        },
        {
          title: "Opscion 4",
          state: true,
        },
        {
          title: "Opscion 5",
          state: true,
        },
      ],
      footer: 2,
    },
  ]);
  const handleChange = (value, id) => {
    const mathc = data.filter((el) => el.id === id)[0];
    const act = mathc.options.filter((el) => el.title === value)[0];
    const copy = [...mathc.options];
    const targetIndex = copy.findIndex((f) => f.title === value);

    if (targetIndex > -1) {
      copy[targetIndex] = { title: value, state: !act.state };
      mathc.options = copy;
    }
    const copyList = [...data];
    const targetIndexList = copyList.findIndex((f) => f.title === id);

    if (targetIndexList > -1) {
      copy[targetIndexList] = mathc;
    }
    setData(copyList);
  };
  const handleReset = (id) => {
    const copy = [...data];
    const targetIndex = copy.findIndex((f) => f.id === id);
    if (targetIndex > -1) {
      const raw = [];
      const options = data.filter((el) => el.id === id)[0].options;
      for (let option of options) {
        raw.push({ title: option.title, state: false });
      }
      copy[targetIndex].options = raw;
    }
    setData(copy);
  };
  return (
    <div className="container">
      <Card />
      {data.map((v, index) => (
        <Submenu
          {...v}
          key={index}
          onChange={handleChange}
          onReset={handleReset}
        />
      ))}
    </div>
  );
}

export default App;
